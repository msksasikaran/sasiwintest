@echo off
:: Start the VMQuickConfig application maximized
start "" /MAX "C:\Users\Public\Desktop\VMQuickConfig.exe"

:: Use Python and pyautogui to interact with the UI
python -c "import pyautogui as pag; pag.click(143, 487, duration=5)"
python -c "import pyautogui as pag; pag.click(155, 554, duration=2)"
python -c "import pyautogui as pag; pag.click(637, 417, duration=2)"
python -c "import pyautogui as pag; pag.click(588, 10, duration=2)"

:: Display the user credentials
echo User name : runneradmin
echo User Pass : TheDisa1a

:: Display AnyDesk ID and password
:: Note: Replace this with the actual method to retrieve AnyDesk ID and password
python -c "import pyperclip; import pyautogui as pag; pag.click(700, 10, duration=2); pag.hotkey('ctrl', 'c'); anydesk_id = pyperclip.paste(); print(f'AnyDesk ID: {anydesk_id}')"
echo AnyDesk Password: TheDisa1a
